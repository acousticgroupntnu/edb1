% Edge diffraction toolbox
% Version EDB11 15-Apr-2012
%
% From version EDB11, all functions will start EDB11 rather than EDB1. The 1 will indicate
% version number.
%
% Main programs
%  EDB1main       	  - Calculates the specular and edge diffraction IRs and saves them in files.
%
% Read CAD file and geometry pre-processing
%  EDB1readsetup       - Runs the EDsetup m-file and saves all the settings in a mat-file.
%  EDB1readcad         - Reads a file of type .CAD and saves all the geometry data in a mat-file. 
%  EDB1readac          - EDB1readac - Reads a file of type .AC (made by e.g. Invis AC3D) and saves all the geometry data in a mat-file. 
%  EDB1ed2geox         - Calculates 2nd- and higher-order edge-related geom. parameters.
%  EDB1edgeox          - Calculates some plane- and edge-related geometrical parameters.
%  EDB1SorRgeo         - Calculates some source- or receiver-related geometrical parameters.
%
% Find sound paths
%  EDB1findpathsISESx  - Finds all possible paths that include direct sound, specular, diffraction.
%  EDB1diff2ISES       - Gives list of paths that includes a 2nd-order diff. combination.
%  EDB1diffISESx       - Gives list of paths that includes a 1st-order diff. combination.
%  EDB1diffNISES       - Gives list of paths that includes an N-order diff. combination.
%  EDB1directsound     - Checks if the direct sound is valid.
%  EDB1findISEStree    - Constructs a list ("an ISES tree") of all possible specular-diffraction combinations.
%  EDB1findis          - Returns the image source coordinates.
%  EDB1checkobstrpaths - Checks obstruction for a list of paths, or check if specular reflections are valid.
%  EDB1checkobstr_pointtoedge      - Checks obstruction for a list of point-to-edge paths.
%  EDB1checkobstr_edgetoedge       - Checks obstruction for a list of edge-to-edge paths.
%  EDB1chkISvisible    - Checks if paths from a set of IS to a set of R pass through their refl. planes.
%  EDB1chkISvisiblex   - Checks if paths from a set of IS to a set of R pass through their refl. planes. Extended version.
%  EDB1getedgepoints   - Calculates a number of edge coordinates.
%  EDB1infrontofplane  - Checks if a point is in front of, in plane with, or behind a list of planes. 
%  EDB1poinpla         - Detects if one or more points are inside a number of finite planes. 
%  EDB1poinplax        - Detects if one or more points are inside a number of finite planes. Extended version.
%  EDB1speculISES      - Finds the valid specular reflections by checking visibility and obstruction.
%
% Create impulse responses
%  EDB1makeirs         - Constructs IRs from a list of paths in an edpathsfile.
%  EDB1betaoverml      - Integrand function which is called for num. int. of ED IR.
%  EDB1irfromslotlist  - Creates an IR from a list of sample numbers and amplitudes
%  EDB1wedge1st_int    - Computes the 1st-order diffraction impulse response.
%  EDB1wedge2nd 		  - Computes the second-order diffraction impulse responses.
%  EDB1wedgeN 		  - Computes the Nth-order diffraction impulse responses.
%  EDB1quadstep        - Recursive numerical integration routine for EDB1wedge1st_int.
%
% Various plot functions
%  EDB1makemovie       - Makes a movie based on IRs.
%  EDB1makesnapshot    - Makes a snapshot of a sound field based on IRs.
%  EDB1plotmodel       - Plots a model which is given in an eddatafile.
%  EDB1plotstackedIRs  - Plots a number of IRs in a stacked way.
%  EDB1plotstackedTFs  - Plots a number of TFs in a stacked way.
%  EDB1plotpath 		  - Plots a model which is given in an eddatafile and one or more of the paths in an edreflpathsfile.
%
% Various string functions
%  EDB1extrnums        - Extracts the numerical values in a text-string, separated by a given delimiter.
%  EDB1fistr           - Reads a text file, line by line, until a given textstring is found.
%  EDB1myvalueinput    - Prints a text on the screen and waits for a numerical to be input.
%  EDB1strpend         - Removes a specified ending from a filename.
%
% Various numerical functions
%  EDB1calcdist        - Gives distances between sets of points in 3D space.
%  EDB1compress7       - 
%  EDB1coordtrans1     - Transforms one set of cartesian coordinates to edge-related cylindrical coordinates.
%  EDB1coordtrans2     - Transforms two sets of cartesian coordinates to edge-related cylindrical coordinates.
%  EDB1creindexmatrix  - Creates a matrix with index numbers.
%  EDB1cross           - Stripped down version of Matlab's built in function cross.
%
% Function for editing paths
%  EDB1editpathlist    - Does some semi-automatic editing of an edpathsfile.
